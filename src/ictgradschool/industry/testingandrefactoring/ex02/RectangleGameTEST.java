package ictgradschool.industry.testingandrefactoring.ex02;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RectangleGameTEST {
    RectangleGame rectangleGame;

    @Before
    public void setUp() {
        rectangleGame = new RectangleGame();
    }

    @Test
    public void testGetAreaRectangle() {
        assertEquals(100, rectangleGame.getAreaRectangle(10, 10));
        assertEquals(200, rectangleGame.getAreaRectangle(20.0, 10.0));
    }

    @Test
    public void testGetAreaCircle() {

        assertEquals(531, rectangleGame.getAreaCircle(13));
        assertEquals(314, rectangleGame.getAreaCircle(10));
    }

    @Test
    public void testCompareAreas() {
//        assertEquals();
        assertEquals(200, rectangleGame.compareAreas(300, 200));
        assertEquals(379, rectangleGame.compareAreas(598, 379));
    }
}
