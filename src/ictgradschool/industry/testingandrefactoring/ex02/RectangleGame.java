package ictgradschool.industry.testingandrefactoring.ex02;

import java.util.Scanner;


public class RectangleGame {

    static Scanner keyboard = new Scanner(System.in);
    // Initialized within the class, but outside of any methods

    public static void main(String[] args) {
        RectangleGame game = new RectangleGame();
        game.run();
    }

    public void run() {

        System.out.println("Welcome to Shape Area Calculator!");

        System.out.print("Enter the width of the rectangle: ");
        double width = keyboard.nextDouble();

        System.out.println("Enter the length of the rectangle: ");
        double length = keyboard.nextDouble();

        System.out.print("The radius of the circle is: ");
        double radius = keyboard.nextDouble();

        int recArea = getAreaRectangle(length, width);
        int circArea = getAreaCircle(radius);
        int compared = compareAreas(recArea, circArea);

        System.out.println("The smaller area is: " + compared);

    }

    public int getAreaRectangle(double length, double width) {
        double area = length * width;
        int round = (int) Math.round(area);
        return round;
    }

    public int getAreaCircle(double radius) {
        double area = Math.PI * (radius * radius);
        int rounded = (int) Math.round(area);
        return rounded;

    }

    public int compareAreas(int recArea, int circArea) {

        return Math.min(recArea, circArea);
    }


}






