package ictgradschool.industry.testingandrefactoring.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testTurnNorth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        myRobot.turn();//East
        myRobot.turn();//South
        myRobot.turn();//West
        myRobot.turn();//North
        assertEquals(Robot.Direction.North, myRobot.getDirection());
    }

    @Test
    public void testTurnEast() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        myRobot.turn();//East
        assertEquals(Robot.Direction.East, myRobot.getDirection());
    }

    @Test
    public void testTurnSouth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        myRobot.turn();//East
        myRobot.turn();//South
        assertEquals(Robot.Direction.South, myRobot.getDirection());
    }

    @Test
    public void testTurnWest() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        myRobot.turn();//East
        myRobot.turn();//South
        myRobot.turn();//West
        assertEquals(Robot.Direction.West, myRobot.getDirection());
    }


    @Test
    public void testValidMoveNorth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());


        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail();
        }
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(9, myRobot.row());
        assertEquals(1, myRobot.column());

    }

    @Test
    public void testValidMoveEast() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        myRobot.turn();

        assertEquals(Robot.Direction.East, myRobot.getDirection());

        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail();
        }
        assertEquals(Robot.Direction.East, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(2, myRobot.column());

    }

    @Test
    public void testValidMoveSouth() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        try {
            myRobot.move();
            assertEquals(Robot.Direction.North, myRobot.getDirection());
            assertEquals(9, myRobot.row());
            assertEquals(1, myRobot.column());

            myRobot.turn();
            myRobot.turn();
            assertEquals(Robot.Direction.South, myRobot.getDirection());

            myRobot.move();
        } catch (IllegalMoveException e) {
            fail("Whoops");
        }

        assertEquals(Robot.Direction.South, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testValidMoveWest() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        myRobot.turn();
        assertEquals(Robot.Direction.East, myRobot.getDirection());

        try {
            myRobot.move();
            assertEquals(Robot.Direction.East, myRobot.getDirection());
            assertEquals(10, myRobot.row());
            assertEquals(2, myRobot.column());

            myRobot.turn();
            myRobot.turn();
            assertEquals(Robot.Direction.West, myRobot.getDirection());

            myRobot.move();
        } catch (IllegalMoveException e) {
            fail("Whoops");
        }

        assertEquals(Robot.Direction.West, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveSouth() {

        // Check that robot has reached the bottom.
        assertEquals(myRobot.currentState().row, Robot.GRID_SIZE);

        try {

            // Now try to continue to move South.
            myRobot.turn();
            myRobot.turn();

            assertEquals(Robot.Direction.South, myRobot.getDirection());

            myRobot.move();
            fail();

        } catch (IllegalMoveException e) {

            assertEquals(10, myRobot.currentState().row);
        }
    }


    @Test
    public void testIllegalMoveEast() {


        myRobot.turn();//East

        assertEquals(Robot.Direction.East, myRobot.getDirection());
        try {

            // Move the robot to the far right row.
            for (int i = 0; i < Robot.GRID_SIZE + 1; i++)
                myRobot.move();

            // Check that robot has reached the far right.
            assertEquals(10, myRobot.currentState().column);

            //try to move east out of grid

            myRobot.move();

            fail();

        } catch (IllegalMoveException e) {

            assertEquals(10, myRobot.currentState().column);
        }

    }


    @Test
    public void testIllegalMoveWest() {


        myRobot.turn();//East
        myRobot.turn();//South
        myRobot.turn();//West

        assertEquals(Robot.Direction.West, myRobot.getDirection());
        // Check that robot is in the far left.
        assertEquals(1, myRobot.currentState().column);

        try {
            //try to move west out of grid

            myRobot.move();

            fail();

        } catch (IllegalMoveException e) {

            assertEquals(1, myRobot.currentState().column);
        }

    }


}
